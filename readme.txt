Need use some command for configure Windows Server:
1. Log onto the server using RDP
2. Open a PowerShell prompt as Administrator and execute the following:
    New-SelfSignedCertificate -DnsName <your_server_dns_name_or_whatever_you_like> -CertStoreLocation Cert:\LocalMachine\My
3. Copy the certificate THUMBPRINT returned by the command to the clipboard.
4. Configure WinRM to listen on 5986. By default, WinRM over HTTP is configured to listed on 5985. We need to enable it on 5986 and bind the certificate.
5. Open a command prompt window as Administrator (not PowerShell).
6. Run the following command, pasting your new certificate’s thumbprint into the command (all on one line):
    winrm create winrm/config/Listener?Address=*+Transport=HTTPS @{Hostname=”<your_server_dns_name_or_whatever_you_like>”; CertificateThumbprint=”<certificate_thumbprint_from powershell>”}

After that, you can use script.sp1 in this repository, that clear site folder, deploy website on Windows Server.

Result you can see in this repository (screenshots). I find website in GitHub

I add CI/CD file.