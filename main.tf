terraform {

   required_version = ">=0.12"

   required_providers {
     azurerm = {
       source = "hashicorp/azurerm"
       version = "~>2.0"
     }
   }
}

 provider "azurerm" {
   features {}
 }

 resource "azurerm_resource_group" "test" {
   name     = "acctestrg"
   #location = "australiaeast"
   location = "australiaeast"
 }

 resource "azurerm_virtual_network" "test" {
   name                = "acctvn"
   address_space       = ["10.0.0.0/16"]
   location            = azurerm_resource_group.test.location
   resource_group_name = azurerm_resource_group.test.name
 }

 resource "azurerm_subnet" "test" {
   name                 = "acctsub"
   resource_group_name  = azurerm_resource_group.test.name
   virtual_network_name = azurerm_virtual_network.test.name
   address_prefixes     = ["10.0.2.0/24"]
 }

 resource "azurerm_public_ip" "test" {
   name                         = "publicIPForLB"
   location                     = azurerm_resource_group.test.location
   resource_group_name          = azurerm_resource_group.test.name
   allocation_method            = "Static"
   domain_name_label            = "mywind-wond-wand"
   #sku                          = "Standard"
 }

 resource "azurerm_public_ip" "myterraformpublic" {
   count = 2
   name                         = "puclicIP-${count.index}"
   location                     = azurerm_resource_group.test.location
   resource_group_name          = azurerm_resource_group.test.name
   allocation_method            = "Dynamic"
   domain_name_label            = "my-wind-${count.index}"
   #sku                          = "Standard"
 }

 resource "azurerm_lb" "test" {
   name                = "loadBalancer"
   location            = azurerm_resource_group.test.location
   resource_group_name = azurerm_resource_group.test.name
   #sku                 = "Standard"

   frontend_ip_configuration {
     name                 = "publicIPAddress"
     public_ip_address_id = azurerm_public_ip.test.id
     #private_ip_address = "10.0.1.10"
     #private_ip_address_allocation = "Static"
   }
 }

 resource "azurerm_lb_backend_address_pool" "test" {
   resource_group_name = azurerm_resource_group.test.name
   loadbalancer_id     = azurerm_lb.test.id
   name                = "BackEndAddressPool"

   depends_on = [
     azurerm_lb.test
   ]
 }

 resource "azurerm_lb_rule" "test" {
   resource_group_name = azurerm_resource_group.test.name
   loadbalancer_id = azurerm_lb.test.id
   name = "LBRule"
   protocol = "tcp"
   frontend_port = 80
   backend_port = 80
   frontend_ip_configuration_name = "publicIPAddress"
   enable_floating_ip = false
   backend_address_pool_id = azurerm_lb_backend_address_pool.test.id
   idle_timeout_in_minutes = 5
   probe_id = azurerm_lb_probe.test.id
   depends_on = [
     azurerm_lb_probe.test
   ]
 }

 resource "azurerm_lb_probe" "test" {
   resource_group_name = azurerm_resource_group.test.name
   loadbalancer_id = azurerm_lb.test.id
   name = "tcpProbe"
   protocol = "tcp"
   port = 80
   interval_in_seconds = 5
   number_of_probes = 2
 }

 resource "azurerm_network_interface" "test" {
   count               = 2
   name                = "acctni${count.index}"
   location            = azurerm_resource_group.test.location
   resource_group_name = azurerm_resource_group.test.name

   ip_configuration {
     name                          = "testConfiguration"
     subnet_id                     = azurerm_subnet.test.id
     private_ip_address_allocation = "Static"
     public_ip_address_id          = azurerm_public_ip.myterraformpublic[count.index].id
     private_ip_address =          var.private_ip[count.index]
   }
 }

 ########################
 resource "azurerm_network_interface_backend_address_pool_association" "test" {
   count = 2
   network_interface_id     = azurerm_network_interface.test[count.index].id
   ip_configuration_name    = "testConfiguration"
   backend_address_pool_id  = azurerm_lb_backend_address_pool.test.id
 }
 ########################

 resource "azurerm_managed_disk" "test" {
   count                = 2
   name                 = "datadisk_existing_${count.index}"
   location             = azurerm_resource_group.test.location
   resource_group_name  = azurerm_resource_group.test.name
   storage_account_type = "Standard_LRS"
   create_option        = "Empty"
   disk_size_gb         = "1023"
 }

 resource "azurerm_availability_set" "avset" {
   name                         = "avset"
   location                     = azurerm_resource_group.test.location
   resource_group_name          = azurerm_resource_group.test.name
   platform_fault_domain_count  = 2
   platform_update_domain_count = 2
   managed                      = true
 }

resource "azurerm_network_security_group" "myterraformnsg" {
  name                = "myNetworkSecurityGroup"
  location            = azurerm_resource_group.test.location
  resource_group_name = azurerm_resource_group.test.name

  security_rule {
    name                       = "SSH"
    priority                   = 1001
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "22"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }

  security_rule {
    name                       = "HTTP"
    priority                   = 1020
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "80"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }

  security_rule {  //Here opened https port
    name                       = "HTTPS"  
    priority                   = 1000  
    direction                  = "Inbound"  
    access                     = "Allow"  
    protocol                   = "Tcp"  
    source_port_range          = "*"  
    destination_port_range     = "443"  
    source_address_prefix      = "*"  
    destination_address_prefix = "*"  
  }

  security_rule {   //Here opened WinRMport
    name                       = "winrm"  
    priority                   = 1010  
    direction                  = "Inbound"  
    access                     = "Allow"  
    protocol                   = "Tcp"  
    source_port_range          = "*"  
    destination_port_range     = "5985"  
    source_address_prefix      = "*"  
    destination_address_prefix = "*"  
  }

  security_rule {   //Here opened WinRMport HTTPS
    name                       = "winrm-https"  
    priority                   = 1030  
    direction                  = "Inbound"  
    access                     = "Allow"  
    protocol                   = "Tcp"  
    source_port_range          = "*"  
    destination_port_range     = "5986"  
    source_address_prefix      = "*"  
    destination_address_prefix = "*"  
  }

  security_rule {   //Here opened https port for outbound
    name                       = "winrm-out"  
    priority                   = 100  
    direction                  = "Outbound"  
    access                     = "Allow"  
    protocol                   = "*"  
    source_port_range          = "*"  
    destination_port_range     = "5985"  
    source_address_prefix      = "*"  
    destination_address_prefix = "*"  
  }

  security_rule {   //Here opened remote desktop port
    name                       = "RDP"  
    priority                   = 110  
    direction                  = "Inbound"  
    access                     = "Allow"  
    protocol                   = "Tcp"  
    source_port_range          = "*"  
    destination_port_range     = "3389"  
    source_address_prefix      = "*"  
    destination_address_prefix = "*"  
  }  
}

 resource "azurerm_windows_virtual_machine" "test" {
   count                 = 2
   name                  = "acctvm${count.index}"
   location              = azurerm_resource_group.test.location
   availability_set_id   = azurerm_availability_set.avset.id
   resource_group_name   = azurerm_resource_group.test.name
   network_interface_ids = [ azurerm_network_interface.test[count.index].id ]
   size                  = "Standard_F2"
   
   #zone = var.number_availabiliti_zones[count.index]


    os_disk {
     name                 = "myOsDisk-${count.index}"
     caching              = "ReadWrite"
     storage_account_type = "Standard_LRS"
    }

    source_image_reference {
     publisher = "MicrosoftWindowsServer"
     offer     = "WindowsServer"
     sku       = "2019-Datacenter"
     version   = "latest"
    }

    connection {
      type      = "winrm"
      port      = 5986
      host      = var.private_ip[count.index]
      user      = "yevhenii"
      password  = "P@$$w0rd1234!"
      https     = true
      insecure  = true
    }

    computer_name                   = "myvm-${count.index}"
    admin_username                  = "yevhenii"
    admin_password                  = "P@$$w0rd1234!"

    tags = {
     environment = "staging"
    }
   
}

resource "azurerm_virtual_machine_extension" "test" {
  count = 2
  depends_on = [
    azurerm_windows_virtual_machine.test
  ]

  name                    = "windows-vm-extension"
  publisher               = "Microsoft.Compute"
  virtual_machine_id      = azurerm_windows_virtual_machine.test[count.index].id
  type                    = "CustomScriptExtension"
  type_handler_version    = "1.9"

  settings = <<SETTINGS
  {
    "commandToExecute": "powershell Install-WindowsFeature -name Web-Server -IncludeManagementTools;"
  }
  SETTINGS
}