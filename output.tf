output "public_ip" {
  value = azurerm_lb.test.private_ip_address
}

output "public_ip_vm" {
  value = azurerm_windows_virtual_machine.test[*].public_ip_address
}

output "private_address" {
  value = azurerm_windows_virtual_machine.test[*].private_ip_address
}