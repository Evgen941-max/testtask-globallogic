﻿$so = New-PsSessionOption –SkipCACheck -SkipCNCheck
$creds = Get-Credential
$session1 = New-PSSession -ComputerName <public_ip>  -Credential $creds -UseSSL -SessionOption $so
$session2 = New-PSSession -ComputerName <public_ip>  -Credential $creds -UseSSL -SessionOption $so

Invoke-Command -ComputerName <public_ip> -ScriptBlock { Remove-Item C:\inetpub\wwwroot\* } -credential $creds
Copy-Item -Path C:\Users\Kolom\Desktop\HELPTECH-master\* -Force -Recurse -Destination "C:\inetpub\wwwroot\" -ToSession $session1

Invoke-Command -ComputerName <public_ip> -ScriptBlock { Remove-Item C:\inetpub\wwwroot\* } -credential $creds
Copy-Item -Path C:\Users\Kolom\Desktop\HELPTECH-master\* -Force -Recurse -Destination "C:\inetpub\wwwroot\" -ToSession $session2