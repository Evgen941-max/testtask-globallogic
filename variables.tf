variable "number_availabiliti_zones" {
  type = list(string)
  default = [ "1", "2" ]
}

variable "private_ip" {
  type = list(string)
  default = [ "10.0.2.4", "10.0.2.5" ]
}